if test "${XDG_SESSION_TYPE}" != 'tty'; then
  export BROWSER=firefox
  case ${XDG_SESSION_TYPE} in
    'wayland' ) export MOZ_ENABLE_WAYLAND=1 ;;
    * )
  esac
fi
