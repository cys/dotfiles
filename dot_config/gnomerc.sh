if ! test -n "$GNOMERC_SH"; then
export GNOMERC_SH=1

GNOMERC_D="${XDG_CONFIG_HOME:=$HOME/.config}/gnomerc.d"

if test -d "$GNOMERC_D" && test -r "$GNOMERC_D"; then
  for file in "$GNOMERC_D/*.sh"; do
    test -r "$file" && . "$file"
  done
fi

unset -v GNOMERC_D

fi
