# TTY
fgconsole 1>/dev/null 2>/dev/null &&
  setfont /usr/share/kbd/consolefonts/ter-118n.psf.gz

_append_path() {
  _var_name="$1"
  _path="$2"

  case $(eval 'printf' "'%s'" '":${'${_var_name}'}:') in
    *:"$_path":* )
    ;;
    * )
      eval ${_var_name}'="${'${_var_name}':+${'${_var_name}'}:}${_path}"'
  esac

  unset -v _var_name
  unset -v _path
}

_insert_path() {
  _var_name="$1"
  _path="$2"

  case $(eval 'printf' "'%s'" '"${'${_var_name}'}"') in
    '' )
      eval ${_var_name}'="${_path}"'
    ;;
    "${_path}" | "${_path}":* )
    ;;
    *:"${_path}" )
      eval ${_var_name}'="${_path}:${'${_var_name}'%:${_path}}"'
    ;;
    *:"${_path}":* )
      eval '_prefix="${'${_var_name}'%%${_path}:*}"'
      eval '_suffix="${'${_var_name}'#*${_path}:}"'
      eval ${_var_name}'="${_path}:${_prefix}${_suffix}"'
      unset -v _prefix
      unset -v _suffix
    ;;
    * )
      eval ${_var_name}'="${_path}:${'${_var_name}'}"'
  esac

  unset -v _var_name
  unset -v _path
}

append_path() {
  _append_path PATH $1
}

insert_path() {
  _insert_path PATH $1
}

append_man() {
  _append_path MANPATH $1
}

insert_man() {
  _insert_path MANPATH $1
}

append_info() {
  _append_path INFOPATH $1
}

insert_info() {
  _insert_path INFOPATH $1
}

# Directories
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CACHE_HOME="$HOME/.cache"
SH_CONFIG_DIR="$XDG_CONFIG_HOME/sh"

# Common env.
if [ "$(uname -o)" = 'GNU/Linux' ]; then
  export DMI_PRODUCT_NAME=$(cat '/sys/devices/virtual/dmi/id/product_name')
fi

# Chezmoi stuff.
insert_path "$HOME/.local/bin"
if [ -d '/mingw64/bin' ]; then
  insert_path '/mingw64/bin'
fi
# . "$SH_CONFIG_DIR/chezmoi.sh"

# Paths.
if [ "$(uname -s)" = 'MSYS_NT-10.0' ]; then
  PYTHON_VERSION=310
  insert_path '/c/Program Files/Mozilla Firefox'
  insert_path '/mingw64/bin'
  insert_man '/mingw64/share/man'
  insert_info '/mingw64/share/info'
  insert_path '/c/Program Files/Python'${PYTHON_VERSION}
  insert_path '/c/Program Files/Python'${PYTHON_VERSION}'/Scripts'
  insert_path '/c/Users/'${USER}'/AppData/Roaming/Python/Python'${PYTHON_VERSION}'/Scripts'
  insert_path '/c/Users/'${USER}'/AppData/Local/Programs/MiKTeX/miktex/bin/x64'
fi

insert_path "$HOME/.local/bin"
insert_path "$HOME/.local/bin/scripts"
insert_path "$HOME/.local/bin/control"

export PATH

# Load profiles from profile.d.
if test -d "$SH_CONFIG_DIR/profile.d"; then
  for profile in "$SH_CONFIG_DIR"/profile.d/*.sh; do
    test -r "$profile" && . "$profile"
  done
fi

# Unload the PATH functions.
unset -f append_path
unset -f insert_path
