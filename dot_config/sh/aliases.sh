# system
alias hibernate='sudo sh -c "sync && echo 3 > /proc/sys/vm/drop_caches &&\
  systemctl hibernate"'

# coreutils
alias ls='ls --color=auto'

# git
alias sdot='sudo git --git-dir=/root/.dotfiles --work-tree=/'

# chezmoi
alias czm='chezmoi -D ~ -S ~/.local/share/chezmoi'
alias czmcd='env -C ~/.local/share/chezmoi $SHELL -i'
alias sczm='sudo chezmoi -D / -S ~/.local/share/schezmoi'
alias sczmcd='env -C ~/.local/share/schezmoi $SHELL -i'

# pacman
alias p='pacman'
alias sp='sudo pacman'
alias spyu='sudo pacman -Syu'
alias spyyu='sudo pacman -Syyu'
alias sp-c='sudo sh -c\
  "paccache -r -k 1 --min-atime '"'"'30 days ago'"'"';"\
  "paccache -r -k 1 --min-atime '"'"'30 days ago'"'"' -u"'

# others
alias v='nvim'
alias c=clear
alias r='ranger'
alias net='netctl-auto switch-to'
alias netl='netctl-auto list'
alias sqlite3='sqlite3 -init "$XDG_CONFIG_HOME/sqlite/init"'
alias sqlite='sqlite3'
alias py='python'
