(defun input-switch-ibus ()
  (defconst ibus-engine-default "xkb:us::eng" "The default I-Bus engine.")

  (defvar ibus-engine ibus-engine-default "The current I-Bus engine.")

  (defun input-switch-enter ()
    "Start using current I-Bus engine."
    (call-process "ibus" nil nil nil "engine" ibus-engine))

  (defun input-switch-exit ()
    "Stop using current I-Bus engine."
    (setq ibus-engine (substring (shell-command-to-string "ibus engine") 0 -1))
    (call-process "ibus" nil nil nil "engine" ibus-engine-default)))

(defun input-switch-fcitx5 ()
  (defvar fcitx5-active nil)

  (defun input-switch-enter ()
    (when fcitx5-active
      (call-process "fcitx5-remote" nil nil nil "-o")))

  (defun input-switch-exit ()
    (setq fcitx5-active
          (equal "2"
                 (substring (shell-command-to-string "fcitx5-remote") 0 -1)))
    (call-process "fcitx5-remote" nil nil nil "-c")))

(when (equal (getenv "XDG_CURRENT_DESKTOP") "KDE") (input-switch-fcitx5))

;; Add hooks.
(add-hook 'evil-insert-state-entry-hook 'input-switch-enter)
(add-hook 'evil-insert-state-exit-hook 'input-switch-exit)

(provide 'input-switch)
