;;; chezmoi --- Functions for chezmoi.

;;; Commentary:
;;; Common functions for chezmoi.

;;; Code:

;;; Load necessary packages.
(require 'term)
(require 'dired)
(require 'diff)
(require 'simple)
(require 'json)

;;; Normal user.
(defvar chezmoi-dir "~/.local/share/chezmoi")
(defvar chezmoi-diff-buffer-name "*chezmoi diff*")
(defvar chezmoi-apply-buffer-name "*chezmoi apply*")
(defvar chezmoi-data
  (json-parse-string
   (shell-command-to-string
    "chezmoi -S ~/.local/share/chezmoi data --format=json"))
  "The chezmoi data structure.")
(defvar chezmoi-os (gethash "os" (gethash "chezmoi" chezmoi-data))
  "Operating system.")
(defvar chezmoi-location (gethash "location" (gethash "options" chezmoi-data))
  "Geographical location.")
(defun chezmoi-cd ()
  "Open chezmoi directory in dired."
  (interactive)
  (dired chezmoi-dir))
(defun chezmoi-apply ()
  "Run 'chezmoi apply'."
  (interactive)
  (let ((term-buffer
	 (make-term chezmoi-apply-buffer-name "chezmoi" nil
		    "-D" "~/" "-S" chezmoi-dir "apply")))
    (switch-to-buffer term-buffer)
    (set-buffer term-buffer)
    (term-mode)
    (term-char-mode)))
(defun chezmoi-diff ()
  "Open 'chezmoi diff' in a new buffer with diff mode."
  (interactive)
  (switch-to-buffer chezmoi-diff-buffer-name)
  (erase-buffer)
  (call-process "chezmoi" nil chezmoi-diff-buffer-name nil
		"-D" "~/" "-S" chezmoi-dir "diff")
  (diff-mode))

;;; Root user.
(defvar chezmoi-root-dir "~/.local/share/schezmoi")
(defvar chezmoi-root-diff-buffer-name "*schezmoi diff*")
(defvar chezmoi-root-apply-buffer-name "*schezmoi apply*")
(defun chezmoi-root-cd ()
  "Open chezmoi directory for root in dired."
  (interactive)
  (dired chezmoi-root-dir))
(defun chezmoi-root-apply ()
  "Run 'schezmoi apply'."
  (interactive)
  (let ((term-buffer
	 (make-term chezmoi-root-apply-buffer-name"chezmoi" nil
		    "-D" "~/" "-S" chezmoi-root-dir "apply")))
    (switch-to-buffer term-buffer)
    (set-buffer term-buffer)
    (term-mode)
    (term-char-mode)))
(defun chezmoi-root-diff ()
  "Open 'schezmoi diff' in a buffer with diff mode."
  (interactive)
  (switch-to-buffer chezmoi-root-diff-buffer-name)
  (erase-buffer)
  (call-process "chezmoi" nil chezmoi-diff-buffer-name nil
		"-D" "/" "-S" chezmoi-root-dir "apply")
  (diff-mode))

(provide 'chezmoi)
;;; chezmoi.el ends here
