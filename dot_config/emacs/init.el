;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; START UP
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; Add load path.
(add-to-list 'load-path (concat user-emacs-directory "lisp"))

;;; Add package-custom.
(require 'package-custom)

;;; Bootstrap use-package.
(package-install-smart 'use-package)

;;; chezmoi.
(use-package chezmoi)

;;; package.
(use-package package
  :after (chezmoi)
  :config
  (setq package-archives
        (if (equal chezmoi-location "PRC")
            '(("gnu" . "https://mirrors.tuna.tsinghua.edu.cn/elpa/gnu/")
              ("melpa" . "https://mirrors.tuna.tsinghua.edu.cn/elpa/melpa/"))
          '(("gnu" . "https://elpa.gnu.org/packages/")
            ("melpa" . "https://melpa.org/packages/")))))

;;; use-package.
(use-package use-package
  :config
  (defun use-package-ensure-smart (name args _state &optional _no-refresh)
    "Function for use-package :ensure keyword."
    (dolist (ensure args)
      (let ((package
             (or (and (eq ensure t) (use-package-as-symbol name))
                 ensure)))
        (when (consp package)
          (use-package-pin-package (car package) (cdr-package))
          (setq package (car package)))
        (package-install-smart package))))
  (setq use-package-ensure-function 'use-package-ensure-smart)
  (setq use-package-always-ensure t))
 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; C SOURCE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; Disable warnings
(setq warning-suppress-types '((comp)))

;;; Disable ring bell.
(setq ring-bell-function 'ignore)

;;; Encoding.
(prefer-coding-system 'utf-8-unix)

;;; Emoji fonts
(dolist (emoji-font-pair
         '(("Note Color Emoji" . 1)
           ("Segoe UI Emoji" . 0.8)))
  (add-to-list 'face-font-rescale-alist emoji-font-pair))
(let ((emoji-font
       (if (equal chezmoi-os "windows")
           "Segoe UI Emoji"
         "Noto Color Emoji")))
  (set-fontset-font t 'unicode (font-spec :family emoji-font)))

;;; Chinese fonts.
(dolist (chinese-font-pair
         '(("Source Han Sans CN" . 1.2)
           ("微软雅黑" . 1.1)))
  (add-to-list 'face-font-rescale-alist chinese-font-pair))
(let ((chinese-font
       (if (equal chezmoi-os "windows")
           "微软雅黑"
         "Source Han Sans CN")))
  (set-fontset-font t 'han (font-spec :family chinese-font)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; PACKAGES
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; ahk-mode.
(when (equal chezmoi-os "windows")
  (use-package ahk-mode))

;;; ccls.
(use-package ccls
  :after (cc-mode)
  :hook ((c-mode c++-mode objc-mode cuda-mode) .
         (lambda () (require 'ccls) (lsp))))

;;; cc-mode.
(use-package cc-mode
  :hook ((c-mode) .
         (lambda () (c-set-style "linux"))))

;;; color-theme-tomorrow.
(use-package color-theme-tomorrow
  :config
  (color-theme-tomorrow--define-theme night-bright)
  (enable-theme 'tomorrow-night-bright))

;;; company
(use-package company
  :demand t
  :bind (:map
         company-active-map
         ;; my binds
         ("<tab>" . company-select-next)
         ("<backtab>" . company-select-previous)
         ("<RET>" . company-complete-selection)
         ("S-<return>" . company-abort)
         ("<right>" . company-complete-common))
  :config
  (setq company-idle-delay 0.05)
  (setq completion-ignore-case t)
  (unless (equal chezmoi-os "windows")
    (global-company-mode)))

;;; dired.
(use-package dired)

;;; display-fill-column-indicator.
(use-package display-fill-column-indicator
  :after (text-mode prog-mode simple)
  :hook ((text-mode prog-mode) .
         (lambda ()
           (set-fill-column 79)
           (display-fill-column-indicator-mode 1))))

;;; elist-mode.
(use-package elisp-mode
  :hook (emacs-lisp-mode . (lambda () (setq indent-tabs-mode nil))))

;;; evil
(use-package evil
  :init
  (setq evil-want-keybinding nil)
  (setq evil-want-C-u-scroll t)
  (setq evil-undo-system 'undo-tree)
  :after (undo-tree)
  :demand t
  :bind (:map
         evil-motion-state-map
         ;; next/previous visual line.
         ("j" . evil-next-visual-line)
         ("<down>" . evil-next-visual-line)
         ("k" . evil-previous-visual-line)
         ("<up>" . evil-previous-visual-line)
         ;; next/previous line.
         ("j" . evil-next-line)
         ("<down>" . evil-next-line)
         ("k" . evil-previous-line)
         ("<up>" . evil-previous-line)
         ;;
         ("gj" . evil-next-visual-line)
         ("g <down>" . evil-next-visual-line)
         ("gk" . evil-previous-visual-line)
         ("g <up>" . evil-previous-visual-line)
         ;; buffers.
         ("<SPC>" . nil) ; Unset <SPC>.
         ("<SPC> k" . kill-buffer)
         ("<SPC> b" . switch-to-buffer)
         ("<SPC> <left>" . previous-buffer)
         ("<SPC> <right>" . next-buffer)
         ("<SPC> f" . find-file)
         ("<SPC> s" . save-buffer)
         ("<SPC> c" . save-buffers-kill-terminal)
         ;; windows.
         ("<SPC> 0" . delete-window)
         ("<SPC> 1" . delete-other-windows)
         ("<SPC> 2" . split-window-below)
         ("<SPC> 3" . split-window-right)
         ("<SPC> r" . window-swap-states)
         ("<SPC> o" . other-window)
         ;; Comment line.
         ("gc" . comment-line)
         :map
         evil-insert-state-map
         ;; Auto comment.
         ("<return>" . comment-indent-new-line))
  :config (evil-mode 1))

;;; evil-collection
(use-package evil-collection
  :after (evil man dired help info)
  :config
  ;; Initialize.
  (evil-collection-init)
  ;; man.
  (evil-collection-define-key 'normal 'Man-mode-map
    (kbd "SPC") nil
    (kbd "u") 'scroll-down-command
    (kbd "d") 'scroll-up-command)
  ;; dired.
  (evil-collection-define-key 'normal 'dired-mode-map
    (kbd "SPC") nil
    (kbd "h") 'dired-up-directory
    (kbd "l") 'dired-find-file)
  ;; help.
  (evil-collection-define-key 'normal 'help-mode-map
    (kbd "SPC") nil)
  ;; info.
  (evil-collection-define-key 'normal 'Info-mode-map
    (kbd "SPC") nil))

;;; faces.
(use-package faces
  :config
  (set-face-background 'default "black")
  (set-face-foreground 'default "white")
  (when (equal chezmoi-os "windows")
    (set-face-attribute 'default nil :family "Consolas"))
  (set-face-attribute 'default nil
                      :height (if (equal chezmoi-os "windows")
                                  100 90)))

;;; file.
(use-package files
  :config
  ;; Save #*# files to $XDG_CONFIG_HOME/emacs/auto-saves.
  (setq auto-save-file-name-transforms
        '((".*" "~/.config/emacs/auto-saves/" t)))
  ;; Save *~ files to $XDG_CONFIG_HOME/emacs/backups.
  (setq backup-directory-alist
        '(("." . "~/.config/emacs/backups"))))

;;; fish-mode.
(use-package fish-mode)

;;; flycheck.
(use-package flycheck)

;;; help.
(use-package help)

;;; info.
(use-package info)

;;; input-switch.
(use-package input-switch
  :after (evil))

;;; ivy
(use-package ivy
  :demand t
  :bind (:map
         ivy-minibuffer-map
         ;; My bindings.
         ("<tab>" . ivy-next-line)
         ("<backtab>" . ivy-previous-line)
         ("<RET>" . ivy-done)
         ("S-<return>" . ivy-immediate-done)
         ("<right>" . ivy-partial-or-done))
  :config
  (ivy-mode 1))

;;; js.
(use-package js
  :hook ((js-mode) .
         (lambda () (setq indent-tabs-mode nil)))
  :config
  (setq js-indent-level 4))

(use-package lsp-mode
  :after (evil)
  :demand t
  :config
  (setq lsp-keep-workspace-alive nil)
  :bind (:map
         evil-motion-state-map
         ;; workspaces
         ("C-c C-l wD" . lsp-disconnect)
         ("C-c C-l wd" . lsp-describe-session)
         ("C-c C-l ws" . lsp)
         ;; folders
         ("C-c C-l fa" . lsp-workspace-folders-add)
         ("C-c C-l fb" . lsp-workspace-blacklist-remove)
         ("C-c C-l fr" . lsp-workspace-folders-remove)
         ;; goto
         ("gD" . nil)
         ("gD" . lsp-find-declaration)
         ("gd" . nil)
         ("gd" . lsp-find-definition)
         :map
         evil-normal-state-map
         ;; formatting
         ("=" . nil)
         ("==" . lsp-format-buffer)
         ("=r" . lsp-format-region)))

;;; lsp-pylsp.
(use-package lsp-pylsp
  :after (lsp-mode)
  :demand t
  :config
  (setq lsp-pylsp-plugins-yapf-enabled t)
  ; (setq lsp-pylsp-plugins-autopep8-enabled t)
  (setq lsp-pylsp-plugins-pyflakes-enabled t))

;;; lua-mode.
(use-package lua-mode
  :hook ((lua-mode) .
         (lambda () (setq indent-tabs-mode nil)))
  :config
  (setq lua-indent-level 4))

;;; magit
(use-package magit
  :config
  (when (equal chezmoi-os "windows")
    (setq magit-git-executable "C:\\msys64\\usr\\bin\\git.exe")))

;;; man.
(use-package man)

;;; message.
(unless (equal chezmoi-os "windows")
  (use-package message
    :after (smtpmail)
    :config
    (setq message-send-mail-function 'smtpmail-send-it)))

;;; mu4e.
(unless (equal chezmoi-os "windows")
  (use-package mu4e
    :after (smtpmail)
    :config
    ;; View.
    (setq mu4e-compose-in-new-frame nil)
    ;; Behaviour.
    (setq mu4e-get-mail-command "mbsync -a")
    ;; Contexts.
    (setq mu4e-context-policy 'pick-first
          mu4e-compose-context-policy 'always-ask
          mu4e-contexts
          (list
           (make-mu4e-context
            :name "disroot.org"
            :enter-func (lambda ()
                          (mu4e-message "entering context disroot.org"))
            :leave-func (lambda ()
                          (mu4e-message "leaving context disroot.org"))
            :match-func (lambda (msg)
                          (when msg
                            (mu4e-message-contact-field-matches
                             msg '(:from :to :cc :bcc) "chengys@disroot.org")))
            :vars '((user-mail-address . "chengys@disroot.org")
                    (user-full-name . "Yushun Cheng")
                    (mu4e-sent-folder . "/disroot/Sent")
                    (mu4e-refile-folder . "/disroot/Archive")
                    (mu4e-trash-folder . "/disroot/Trash")
                    (mu4e-drafts-folder . "/local/Drafts")
                    (smtpmail-smtp-user . "chengys")
                    (smtpmail-smtp-service . 587)
                    (smtpmail-smtp-server . "disroot.org")))))))

;;; org
(use-package org
  :config
  (setq org-startup-indented t)
  (set-face-attribute 'outline-1 nil :height 150)
  (set-face-attribute 'outline-2 nil :height 140)
  (set-face-attribute 'outline-3 nil :height 130)
  (set-face-attribute 'outline-4 nil :height 120)
  (set-face-attribute 'outline-5 nil :height 110)
  (set-face-attribute 'outline-6 nil :height 100))

;;; prog-mode.
(use-package prog-mode)

;;; rustic.
(use-package rustic
  :demand t
  :hook ((rust-mode) .
         (lambda () (setq tab-width 4) (setq indent-tabs-mode nil)))
  :bind (:map
         rustic-mode-map
         ("C-c b" . rustic-cargo-build)
         ("C-c c" . rustic-cargo-check)
         ("C-c t" . rustic-cargo-test)
         ("C-c r" . rustic-cargo-run))
  :config
  (setq lsp-rust-analyzer-diagnostics-disabled ["inactive-code"]))

;;; sh-script.
(use-package sh-script
  :hook ((sh-mode) . (lambda () (setq indent-tabs-mode nil)))
  :config
  (setq sh-basic-offset 2))

;;; simple.
(use-package simple)

;;; smtpmail.
(unless (equal chezmoi-os "windows")
  (use-package smtpmail
    :config
    (setq smtpmail-local-domain (system-name))))

;;; text-mode
(use-package text-mode)

;;; tool-bar.
(use-package tool-bar
  :config
  (tool-bar-mode -1))

;;; undo-tree
(use-package undo-tree
  :config
  (let ((dir (concat user-emacs-directory "undo-tree")))
    (make-directory dir t)
    (setq undo-tree-history-directory-alist
          `(("." . ,dir))))
  (global-undo-tree-mode))

;;; yaml-mode.
(use-package yaml-mode)
