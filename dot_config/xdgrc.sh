if ! test -n "$XDGRC_SH"; then
export XDGRC_SH=1

# Source profiles.
XDGRC_DIR=${XDG_CONFIG_HOME}'/xdgrc.d'
if test -d ${XDGRC_DIR}; then
  for profile in ${XDGRC_DIR}'/'*'.sh'; do
    . ${profile}
  done
fi
unset -v XDGRC_DIR

fi
